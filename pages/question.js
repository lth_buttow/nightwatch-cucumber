
var questionActions = {
    with: function (email, msg) {
        return this
            .navigate()
            .waitForElementVisible('@form', 10000)
            .setValue('@emailInput', email)
            .setValue('@msgTextArea', msg)
            .click('@sendButton')
    },
    expectAlertDanger: function (texto) {
        return this
            .waitForElementVisible('@alertDanger', 10000)
            .assert.containsText('@alertDanger', texto)
    },
    expectAlertInfo: function (texto) {
        return this
            .waitForElementVisible('@alertInfo', 10000)
            .assert.containsText('@alertInfo', texto)
    },
    expectStatus: function(msgStatus) {
        return this
            .waitForElementVisible('@statusAlert', 10000)
            .assert.containsText('@statusAlert', msgStatus)
    }
}

module.exports = {
    url: '/',
    commands: [questionActions],
    elements: {
        form: '#contactForm',
        emailInput: '#email',
        msgTextArea: '#message',
        sendButton: '#enviaEmail',
        statusAlert: '#alert',
    }
}