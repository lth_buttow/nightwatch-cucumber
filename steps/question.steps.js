const { client } = require('nightwatch-api')
const { When, Then } = require('cucumber')

When('eu envio uma pergunta com {string} e {string}', function (email, msg) {
    let question = client.page.question()

    return question.with(email, msg)
    
});
Then('devo ver uma mensagem de {string}', function (msgStatus) {
    let question = client.page.question()

    return question.expectStatus(msgStatus)
});