
module.exports = {
    src_folders: ["steps"], //tests is a folder in workspace which has the step definitions
    page_objects_path: 'pages/', //page_objects folder where selectors are saved
    test_settings: {
        default: {
            screenshots: {
                enabled: true,
                path: 'screenshots'
            },
            launch_url: 'https://projetocomun.com/',
            webdriver: {
                start_process: true,
                server_path: 'chrome/chromedriver',
                port: 9515
                // server_path: 'node_modules/.bin/chromedriver',

            },
            desiredCapabilities: {
                browserName: "chrome",
                chromeOptions: {
                    w3c: false,
                    args: ['--headless', '--no-sandbox', '--disable-dev-shm-usage']
                }
            }
        },

        headless: {
            screenshots: {
                enabled: true,
                path: 'screenshots'
            },
            launch_url: 'https://projetocomun.com/',
            webdriver: {
                start_process: true,
                server_path: 'chromedriver.path',
                port: 9515
            },
            desiredCapabilities: {
                browserName: "chrome",
                chromeOptions: {
                    w3c: false,
                    args: ['--headless', '--no-sandbox', '--disable-dev-shm-usage']
                }
            }
        },

        default2: {
            screenshots: {
                enabled: true,
                path: 'screenshots'
            },
            launch_url: 'https://projetocomun.com/',
            webdriver: {
                start_process: true,
                server_path: 'chromedriver.path',
                port: 9515
            },
            desiredCapabilities: {
                browserName: 'chrome'
            }
        },
    }
};