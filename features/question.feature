#language:pt

Funcionalidade: Enviar Pergunta

    Cenário: Pergunta enviada com sucesso

        Quando eu envio uma pergunta com "lkbuttow@gmail.com" e "tenho uma dúvida"
        Então devo ver uma mensagem de "Sucesso! Sua mensagem foi enviada, retornaremos em breve!"
