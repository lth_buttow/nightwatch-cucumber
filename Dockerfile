FROM node:13.6.0-alpine3.11

WORKDIR /

RUN echo "http://dl-4.alpinelinux.org/alpine/v3.9/main" >> /etc/apk/repositories && \
  echo "http://dl-4.alpinelinux.org/alpine/v3.9/community" >> /etc/apk/repositories && \
  apk update && \
  apk add build-base \
          libxml2-dev \
          libxslt-dev \
          curl unzip libexif udev chromium chromium-chromedriver wait4ports xvfb xorg-server dbus ttf-freefont mesa-dri-swrast

COPY nightwatch-cucumber/package* /

RUN npm install

WORKDIR /nightwatch-cucumber

ENTRYPOINT ["npm", "install"]
